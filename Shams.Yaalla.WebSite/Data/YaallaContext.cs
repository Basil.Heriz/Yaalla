﻿using Microsoft.EntityFrameworkCore;
using Shams.Yaalla.WebSite.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shams.Yaalla.WebSite.Data
{
    public class YaallaContext : DbContext
    {
        public YaallaContext()
        {

        }

        public YaallaContext(DbContextOptions<YaallaContext> options)
            : base(options)
        {
         
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Item>()
                .HasIndex(u => u.Code)
                .IsUnique();
        }
        public DbSet<Item> Items { get; set; }
        public DbSet<ItemHistory> ItemHistories { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
