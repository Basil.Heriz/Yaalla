﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Shams.Yaalla.WebSite.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Shams.Yaalla.WebSite.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly Services.IShoterService _shoterService;
        private readonly Services.IUserService _userService;
        public HomeController(ILogger<HomeController> logger,
            Services.IShoterService shoterService,
            Services.IUserService userService)
        {
            _logger = logger;
            _shoterService = shoterService;
            _userService = userService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        public async Task<IActionResult> AddLink(string link)
        {
            Guid userId = Guid.TryParse(HttpContext.Request.Cookies["yaalla-id"], out userId) ? userId : Guid.Empty;

            if (userId == Guid.Empty)
            {
                var addUserResult = await _userService.AddAsync(new Models.Domain.User
                {

                });
                if (!addUserResult.Result)
                {
                    return Ok(new OperationResult { ErrorMessage = "one or more error has occurred" });
                }

                userId = addUserResult.Response.Id;
                HttpContext.Response.Cookies.Append("yaalla-id", userId.ToString());
            }

            var result = await _shoterService.AddAsync(new Models.Dto.AddItem
            {
                Link = link,
                UserId = userId
            });
            return Ok(result);
        }

        public async Task<IActionResult> Link(string query)
        {
            query ??= string.Empty;
            var getResult = await _shoterService.GetAsync(new Models.Dto.GetItem
            {
                Code = query
            });
            if (getResult.Result)
            {
                return Redirect(getResult.Response.OriginalLink);
            }
            else
            {
                return RedirectToAction("Index");
            }

        }
    }
}
