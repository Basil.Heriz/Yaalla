﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shams.Yaalla.WebSite.Models.Domain
{
    public class Item
    {
        public Guid Id { get; set; }
        public DateTime CreationTime { get; set; }
        public string OriginalLink { get; set; }
        public string Code { get; set; }
        public int VistCount { get; set; }
        public Guid CreatorUserId { get; set; }
        public User CreatorUser { get; set; }
        public List<ItemHistory> Histories { get; set; }
    }
}
