﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shams.Yaalla.WebSite.Models.Domain
{
    public class ItemHistory
    {
        public Guid Id { get; set; }
        public Item Item { get; set; }
        public Guid ItemId { get; set; }
        public DateTime CreationTime { get; set; }
        public string Refers { get; set; }
        public string Note { get; set; }
    }
}
