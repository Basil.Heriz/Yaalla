﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shams.Yaalla.WebSite.Models.Dto
{
    public class GetItem
    {
        public string Code { get; set; }
    }
}
