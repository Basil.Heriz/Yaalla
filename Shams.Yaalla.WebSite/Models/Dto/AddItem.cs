﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shams.Yaalla.WebSite.Models.Dto
{
    public class AddItem
    {
        public string Link { get; set; }
        public Guid UserId { get; set; }
    }
}
