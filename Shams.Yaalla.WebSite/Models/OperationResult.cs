﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shams.Yaalla.WebSite.Models
{
    public class OperationResult
    {
        public bool Result { get; set; }
        public string ErrorMessage { get; set; } 
    }


    public class OperationResult<T>: OperationResult
    {
        public T Response { get; set; }
    }
}
