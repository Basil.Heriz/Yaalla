﻿using Shams.Yaalla.WebSite.Models;
using Shams.Yaalla.WebSite.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shams.Yaalla.WebSite.Services
{
    public interface IUserService
    {
        Task<OperationResult<User>> AddAsync(User user);
    }
}
