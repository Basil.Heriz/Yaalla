﻿using Microsoft.Extensions.Logging;
using Shams.Yaalla.WebSite.Models;
using Shams.Yaalla.WebSite.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shams.Yaalla.WebSite.Services
{
    public class UserService : IUserService
    {
        private readonly ILogger<UserService> _logger;
        private readonly Data.YaallaContext _yaallaContext;


        public UserService(ILogger<UserService> logger, Data.YaallaContext yaallaContext)
        {
            _logger = logger;
            _yaallaContext = yaallaContext;
        }

        public async Task<OperationResult<User>> AddAsync(User user)
        {
            OperationResult<User> result = new OperationResult<User>();
            try
            {

                if(string.IsNullOrEmpty(user.Username))
                {
                    user.Username = "guest";
                } 
                user.CreationTime = DateTime.Now;
                _yaallaContext.Users.Add(user);
                await _yaallaContext.SaveChangesAsync();
                result.Response = user;
                result.Result = true;

            }catch(Exception ex)
            {
                _logger.LogError(ex, "Add {@User}", user);
            }
            return result;
        }
    }
}
