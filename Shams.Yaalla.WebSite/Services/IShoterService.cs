﻿using Shams.Yaalla.WebSite.Models;
using Shams.Yaalla.WebSite.Models.Domain;
using Shams.Yaalla.WebSite.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shams.Yaalla.WebSite.Services
{
    public interface IShoterService
    {
        Task<OperationResult<Item>> AddAsync(AddItem item);
        Task<OperationResult<Item>> GetAsync(GetItem item);

    }
}
