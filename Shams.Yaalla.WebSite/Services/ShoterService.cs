﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Shams.Yaalla.WebSite.Helper;
using Shams.Yaalla.WebSite.Models;
using Shams.Yaalla.WebSite.Models.Domain;
using Shams.Yaalla.WebSite.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shams.Yaalla.WebSite.Services
{
    public class ShoterService : IShoterService
    {
        private readonly ILogger<ShoterService> _logger;
        private readonly Yaalla.WebSite.Data.YaallaContext _yaallaContext;

        public ShoterService(ILogger<ShoterService> logger, Data.YaallaContext yaallaContext)
        {
            _logger = logger;
            _yaallaContext = yaallaContext;
        }

        public async Task<OperationResult<Item>> AddAsync(AddItem item)
        {
            OperationResult<Item> result = new OperationResult<Item>();
            try
            {
                var codeGenerated = false;
                var code = string.Empty;
                do
                {
                    code = CodeGenerator.GenerateCode(8);
                    codeGenerated = 
                        string.IsNullOrEmpty(
                            await _yaallaContext.Items
                                        .Where(i => i.Code == code)
                                        .Select(c => c.Code)
                                        .FirstOrDefaultAsync());

                } while (!codeGenerated);

                var newItem = new Item
                {
                    Code = code,
                    CreationTime = DateTime.Now,
                    CreatorUserId = item.UserId,
                    OriginalLink = item.Link,
                    VistCount = 0,
                    Histories = new List<ItemHistory>
                    {
                        new ItemHistory
                        {
                            CreationTime = DateTime.Now,
                            Refers = "",
                            Note ="Link Created"
                        }
                    }
                };

                _yaallaContext.Items.Add(newItem);
                await _yaallaContext.SaveChangesAsync();
                result.Result = true;
                result.Response = newItem;
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "Add {@Item}", item);
            }
            return result;
        }

        public async Task<OperationResult<Item>> GetAsync(GetItem item)
        {
            OperationResult<Item> result = new OperationResult<Item>();
            try
            {
                result.Response = await _yaallaContext.Items
                                            .FirstOrDefaultAsync(i => i.Code == item.Code);
                if (result.Response != null)
                {
                    result.Result = true;
                    result.Response.VistCount++;

                    _yaallaContext.ItemHistories.Add(new ItemHistory
                    {
                        CreationTime = DateTime.Now,
                        ItemId = result.Response.Id,
                        Note = "New visit"
                    });
                    await _yaallaContext.SaveChangesAsync();

                }
                else
                {
                    result.ErrorMessage = "Not Found!";
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Get {@Item}", item);
            }
            return result;
        }
    }
}
