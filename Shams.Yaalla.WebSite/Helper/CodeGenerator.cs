﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shams.Yaalla.WebSite.Helper
{
    public class CodeGenerator
    {

        public const string _bigLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public const string _smallLetters = "abcdefghijklmnopqrstuvwxyz";
        public const string _numbers = "0123456789";

        /// <summary>
        /// Generate Code with the given length and only consists of digits
        /// </summary>
        /// <param name="length">
        /// the length of the generated code</param>
        /// <returns> the generated code</returns>
        public static string GenerateNumberCode(int length)
        {

            return GenerateCode(length, _numbers);
        }

        /// <summary>
        /// Generate codes that consists of English alphabet letters (big and small)
        /// 
        /// </summary>
        /// <param name="length"> length of code</param>
        /// <param name="onlyBigLetters"> set to true if you only Big Letters are desired, default value false</param>
        /// <returns>generated code</returns>
        public static string GenerateLettersCode(int length, bool onlyBigLetters = false)
        {
            if (onlyBigLetters)
            {
                return GenerateCode(length, _bigLetters);
            }
            else
            {
                return GenerateCode(length, _bigLetters + _smallLetters);
            }

        }

        /// <summary>
        /// Generate codes that consists of characters from input string if provided or (letters , big letters, small letters) if input string is empty
        /// </summary>
        /// <param name="length"> length of code</param>
        /// <param name="inputString">input string to generate random characters from, default value "" </param>
        /// <returns>generated code</returns>
        public static string GenerateCode(int length, string inputString = "")
        {
            if (string.IsNullOrEmpty(inputString))
            {
                inputString = _bigLetters + _numbers + _smallLetters;
            }

            Random random = new Random();

            return new string(Enumerable.Repeat(inputString, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
